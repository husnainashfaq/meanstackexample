/**
 * Created by Husnain on 6/3/2015.
 */
var mongoose = require('mongoose');

var schema = new mongoose.Schema(
    {
        bus_number: {type: String, required: true, unique: true},
        driver    : {
            age       : {type: Number},
            first_name: {type: String},
            last_name : {type: String}
        },
        capacity  : {type: Number}
    }
);

module.exports = mongoose.model('buses', schema);
