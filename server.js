'use strict';

/**
 * Module dependencies.
 */
var express  = require('express'),
    fs       = require('fs'),
    mongoose = require('mongoose');

/**
 * Main application entry file.
 * Please note that the order of loading is important.
 */

    // Initializing system variables
var config = require('./config/config');
var db = mongoose.connect(config.dbConfig.driver + '://' + config.dbConfig.host + ':' + config.dbConfig.port + '/' + config.dbConfig.dbName, config.dbOptions, function (err) {
    if (err) {
        console.error("[server] Unable to connect to MongoDB using connection string:" + config.dbConfig.driver + '://' + config.dbConfig.host + ':' + config.dbConfig.port + '/' + config.dbConfig.dbName);
        conosle.error(err);
        throw err;
    }
});
mongoose.connection.on('connected', function () {
    console.info("[server] MongoDB connection established successfully");
});

mongoose.connection.on('error', function (err) {
    console.error('[server] Connection to mongo failed ', err);
});

mongoose.connection.on('disconnected', function () {
    console.warn('[server] MongoDB connection closed');
});
//Bootstrap models
var models_path = __dirname + '/app/models';
var walk        = function (path) {
    fs.readdirSync(path).forEach(function (file) {
        var newPath = path + '/' + file;
        var stat    = fs.statSync(newPath);
        if (stat.isFile()) {
            if (/(.*)\.(js|coffee)/.test(file)) {
                require(newPath);
            }
        } else if (stat.isDirectory()) {
            walk(newPath);
        }
    });
};
walk(models_path);

var app = express();

//express settings
require('./config/express')(app, db);

//Bootstrap routes
require('./config/routes')(app);

//Start the app by listening on <port>
var port = config.port;
app.listen(port);
console.log('Express app started on port ' + port);

//expose app
exports = module.exports = app;
