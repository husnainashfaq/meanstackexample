var async         = require('async'),
    busController = require('../controllers/busContoller');

module.exports = function (app) {
    var buses = [{number: "LES-1224", driver: {first_name: "General", last_name: "Mushi", age: 50}},
        {number: "RWP-3444", driver: {first_name: "Sheikh", last_name: "Fahad", age: 50}}];
    //Home route
    var index = require('../app/controllers/index');
    app.get('/', index.render);
    // route to get all buses
    app.get('/buses', busController.getAllBuses);
    //route to get one bus
    app.get('/bus/:bus_number', busController.getBus);
    //update bus data on server
    app.post('/bus/:bus_number', busController.updateBus);

    app.post('/bus/add/:bus_number', busController.addBus);
};
