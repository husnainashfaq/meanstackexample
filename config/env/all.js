'use strict';

var path     = require('path'),
    rootPath = path.normalize(__dirname + '/../..');

module.exports = {
    root: rootPath,
    port: process.env.PORT || 3000,
    db  : process.env.MONGOHQ_URL,

    // Template Engine
    templateEngine: 'jade',

    // The secret should be set to a non-guessable string that
    // is used to compute a session hash
    sessionSecret    : 'MEAN',
    // The name of the MongoDB collection to store sessions in
    sessionCollection: 'sessions',
    dbConfig         : {
        driver: 'mongodb',
        host  : process.env.DB_SERVER ? process.env.DB_SERVER : '127.0.0.1',
        port  : '27017',
        dbName: 'buses'
    },
    dbOptions        : {
        db    : {native_parser: true},
        server: {
            poolSize     : 5,
            socketOptions: {keepAlive: 1}
        }
    }
}
