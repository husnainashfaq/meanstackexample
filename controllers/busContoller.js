/**
 * Created by Husnain on 12/18/2015.
 */
var busModel        = require("../models/busModel");
/**
 * Function to get all busses
 */
exports.getAllBuses = function (req, res) {
    //to get all buses use find instead of findOne, and do not specify selection criteria
    busModel.find({}, function (error, busesArray) {
        if (error) {
            //something is wrong.. and we don't know.. so tell client.. error
            res.json({success: false, msg: 'unknown error'});
        }
        // now check if bus with number sent by client existed or not..
        else if (busesArray) {
            // yes we found buses.. lets send it client back
            res.json({success: true, data: busesArray});
        }
        //so this means no error, and no buses in db
        else {
            res.json({success: false, msg: 'buses not found'});
        }
    });
};
/**
 * Function to get bus
 * @param req
 * @param res
 */
exports.getBus      = function (req, res) {
    // as we stored dynamic param of bus number in bus_number.... see route file... so get that number first from request
    var busNumber = req.params.bus_number;
    //now search for that bus
    busModel.findOne({number: busNumber}, function (error, busDoc) {
        if (error) {
            //something is wrong.. and we cant know.. so tell client.. error
            res.json({success: false, msg: 'unknown error'});
        }
        // now check if bus with number sent by client existed or not..
        else if (busDoc) {
            // yes we found bus.. lets send it client back
            res.json({success: true, data: busDoc});
        }
        //so this means no error, and no bus... no bus have this humber
        else {
            res.json({success: false, msg: 'bus not found'});
        }
    });
};
// update bus in database
exports.updateBus = function (req, res) {
    //first bus number from request
    var busNumber = req.params.bus_number;
    //now get that buss
    busModel.findOne({bus_number: busNumber}, function (error, busDoc) {
        if (error) {
            console.error("[busController][updateBus] error", error);
        }
        else if (busDoc) {
            if (req.body.driver_first_name) {
                busDoc.driver.first_name = req.body.driver_first_name;
            }
            if (req.body.driver_last_name) {
                busDoc.driver.last_name = req.body.driver_last_name;
            }
            if (req.body.driver_age && !isNaN(parseInt(req.body.driver_age))) {
                busDoc.driver.age = parseInt(req.body.driver_age);
            }
            if (req.body.number) {
                busDoc.number = req.body.number;
            }
            //now we updated its properties.. lets save it
            busDoc.save(function (error) {
                if (error) {
                    console.error("[busController][updateBus] unable to save bus.. error:", error);
                }
                else {
                    res.json({success: true, data: busDoc});

                }
            });
        }
        else {
            res.json({success: true});
        }
    });
};
/**
 * Add bus to our bus collection
 * @param req
 * @param res
 */
exports.addBus    = function (req, res) {
    var busNumber = req.params.bus_number, age, capacity;
    // first age is sent in request. try to parse it to int
    if (req.body.driver_age && !isNaN(parseInt(req.body.driver_age))) {
        age = parseInt(req.body.driver_age);
    }
    // first age is sent in request. try to parse it to int
    if (req.body.capacity && !isNaN(parseInt(req.body.capacity))) {
        capacity = parseInt(req.body.capacity);
    }
    //create new bus using our bus mode..
    var busDoc = new busModel({
        bus_number: busNumber,
        capacity  : capacity,
        driver    : {first_name: req.body.driver_first_name, last_name: req.body.driver_last_name, age: age} // if age is undefined.. it will not be saved.
    });

    //now save newly created bus document
    busDoc.save(function (error) {
        if (error) {
            res.json({success: false, msg: 'unknown error'});
            console.error("[busController][updateBus] unable to save bus.. error:", error);
        }
        else {
            res.json({success: true});

        }
    })
};
